document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();

    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');

    if (taskInput.value.trim() === '') {
        alert('Please enter a task');
        return;
    }

    const taskElement = document.createElement('li');
    taskElement.classList.add('bg-white', 'p-2', 'flex', 'justify-between', 'items-center');
    taskElement.innerHTML = `
        <span>${taskInput.value}</span>
        <div>
            <button class="complete-task bg-green-500 text-white p-1 mr-2"><i class="fas fa-check"></i></button>
            <button class="delete-task bg-red-500 text-white p-1"><i class="fas fa-trash"></i></button>
        </div>
    `;

    taskList.appendChild(taskElement);

    taskInput.value = '';

    taskElement.querySelector('.complete-task').addEventListener('click', function() {
        taskElement.querySelector('span').classList.add('line-through', 'text-gray-500');
    });

    taskElement.querySelector('.delete-task').addEventListener('click', function() {
        taskList.removeChild(taskElement);
    });
});
